// 实现一个 setTimeout 同步方法
function sleep(second) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, second * 1000)
  })
}

(async () => {
  console.log(new Date().toLocaleString())
  await sleep(3)
  console.log(new Date().toLocaleString())
})()