/**
 * 输入为Iterator类型的参数，可以是Array，Map， Set，String ，可能也得包括魔改的Iterator（Symbol.iterator）之类
 * 若输入的可迭代数据里不是Promise，则也需要原样输出
 * 返回一个Promise实例，可以调用then和catch方法
 * 输出在then里体现为保持原顺序的数组
 * 输出在catch体现为最早的reject返回值
 * 空 Iterator， resolve返回空数组
 * 
 * from https://zhuanlan.zhihu.com/p/362648760
 */

function promiseAll(args) {
  return new Promise((resolve, reject) => {
    const result = []
    let iteratorIndex = 0
    let fullCount = 0
    for(const item of args) {
      let resultIndex = iteratorIndex
      iteratorIndex += 1

      Promise.resolve(item).then(res => {
        result[resultIndex] = res
        fullCount += 1
        if(fullCount === iteratorIndex) {
          resolve(result)
        }
      }).catch(err => {
        console.log(err)
        reject(err)
      })
    }

    if(iteratorIndex === 0) {
      resolve(result)
    }
  })
}

let args = []
for(let i = 0; i < 3; i++) {
  const promise = new Promise((resolve, reject) => {
    // reject(i)
    resolve(i)
  })
  args.push(promise)
}

promiseAll(args).then(res => {
  console.log(res)
}).catch(e => {
  console.log(e)
})

// Promise.all(args).then(res => {
//   console.log(res)
// }).catch(e => {
//   console.log(e)
// })