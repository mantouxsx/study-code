
// XMLHttpRequest
const xhr = new XMLHttpRequest()

/** 
 * xhr.readyState
 * 0 未初始化
 * 1 载入
 * 2 载入完成
 * 3 交互
 * 4 完成
 * */

// GET
xhr.open('GET', 'https://xxx', true)
xhr.onreadystatechange = function() {
  if(xhr.readyState === 4) {
    if(xhr.status === 200) {
      console.log(JSON.parse(xhr.responseText))
    } else {
      console.log('其他情况')
    }
  }
}
xhr.send(null)

// POST
xhr.open('POST', 'https://xxx', true)
xhr.onreadystatechange = function() {
  if(xhr.readyState === 4) {
    if(xhr.status === 200) {
      console.log(JSON.parse(xhr.responseText))
    } else {
      console.log('其他情况')
    }
  }
}
const postData = {}
xhr.send(JSON.stringify(postData))

/**
 * xhr.status
 * 2xx 表示成功 200
 * 3xx 重定向，浏览器直接跳转 301 302 304
 * 4xx 客户端报错 404 403
 * 5xx 服务器
 */