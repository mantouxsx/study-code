// - 手写深拷贝
function deepClone(obj = {}) {

}

// test
// let a = {
//   a:[1, 2, 3, 4],
//   b: {
//     c: 1,
//     d: 2
//   }
// }
// let b = deepClone(a)
// b.a[0] = 0
// b.b.c = 2
// console.log(a, b)

// - 手写节流函数
function throttle(fn, delay = 100) {

}

// - 手写防抖函数
function debounce(fn, delay = 500) {

}

// - 手写 Promise 方法
function PromiseM(fn) {

}

// - 手写 Promise.all 方法
function promiseAll(args) {
  
}
