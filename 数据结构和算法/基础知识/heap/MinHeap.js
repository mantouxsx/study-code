// 最小堆类
class MinHeap{
  constructor() {
    this.heap = []
  }
  // 获取父元素下标
  getParentIndex(i) {
    // return Math.floor((i - 1) / 2)
    return (i - 1) >> 1 // 二进数数右移一位
  }
  // 获取左侧子节点
  getLeftIndex(i) {
    return i * 2 + 1
  }
  // 获取右侧子节点
  getRightIndex(i) {
    return i * 2 + 2
  }
  // 交换两个位置的值
  swap(i1, i2) {
    const temp = this.heap[i1]
    this.heap[i1] = this.heap[i2]
    this.heap[i2] = temp
  }
  // 上移操作
  shiftUp(index) {
    if(index === 0) return
    const parentIndex = this.getParentIndex(index)
    if(this.heap[parentIndex] > this.heap[index]) {
      this.swap(parentIndex, index)
      this.shiftUp(parentIndex)
    } 
  }
  // 下移操作
  shiftDown(index) {
    const leftIndex = this.getLeftIndex(index)
    const rightIndex = this.getRightIndex(index)
    if(this.heap[leftIndex] < this.heap[index]) {
      this.swap(leftIndex, index)
      this.shiftDown(leftIndex)
    }
    if(this.heap[rightIndex] < this.heap[index]) {
      this.swap(rightIndex, index)
      this.shiftDown(rightIndex)
    }  
  }
  /**
   * 插入
   * - 将值插入堆的底部，即数组的尾部
   * - 然后上移：将这个值和它的父节点进行交换，直到父节点小于等于这个插入的值
   * - 大小为 k 的堆中插入元素的时间复杂度为 O(logk)
   */
  insert(value) {
    this.heap.push(value)
    this.shiftUp(this.heap.length - 1)
  }
  /**
   * 删除堆顶
   * - 用数组尾部元素替换堆顶
   * - 然后下移：将新堆顶和它的子节点进行交换，直到子节点大于等于这个新堆顶
   * - 大小为 k 的堆中删除的时间复杂度为 O(logk)
   */
  pop() {
    this.heap[0] = this.heap.pop()
    this.shiftDown(0)
  }
  /**
   * 获取堆顶
   */
  peek() {
    return this.heap[0]
  }
  /**
   * 获取堆的大小
   */
  size() {
    return this.heap.length
  }
}

const h = new MinHeap()
h.insert(3)
h.insert(2)
h.insert(1)

h.pop()
