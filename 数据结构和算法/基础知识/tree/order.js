const bt = require("./bt");

/**
 * 深度遍历算法
 * @params root
 * */
const dfs = (root) => {
  if(!root) return
  console.log(root.val)
  // 树
  // root.children.forEach(dfs);
  // 二叉树
  dfs(root.left)
  dfs(root.right)
}

// dfs(bt)

/**
 * 广度优先遍历算法
 * @params root
 * */
const bfs = (root) => {
  const q = [root]
  while(q.length > 0) {
    const n = q.shift()
    console.log(n.val)
    // 树
    // n.children.forEach(child => {
    //   q.push(child)
    // })
    // 二叉树
    if(n.left) q.push(n.left)
    if(n.right) q.push(n.right)
  }
}

bfs(bt)


/**
 * 先序遍历算法
 * @params root
 * */
// 递归
// const preorder = (root) => {
// 	if(!root) return;
//   console.log(root.val)
//   preorder(root.left)
//   preorder(root.right)
// }

// 非递归
const preorder = (root) => {
  if (!root) return;
  const stack = [root];
  while (stack.length) {
    const n = stack.pop();
    console.log(n.val);
    if (n.right) stack.push(n.right);
    if (n.left) stack.push(n.left);
  }
};

// preorder(bt);

/**
 * 中序遍历算法
 * @params root
 * */
// const inorder = (root) => {
//   if (!root) return;
//   inorder(root.left);
//   console.log(root.val);
//   inorder(root.right);
// };

const inorder = (root) => {
  if(!root) return
  const stack = []
  let p = root
  while(stack.length || p) {
    while(p) {
      stack.push(p)
      p = p.left
    } 
    const n = stack.pop()
    console.log(n.val)
    p = n.right
  }
}

// inorder(bt);

/**
 * 后序遍历算法
 * @params root
 * */
// const postorder = (root) => {
// 	if(!root) return
//   postorder(root.left)
//   postorder(root.right)
//   console.log(root.val)
// }

// 后序遍历顺序倒过来 根-右-左
// 使用先序遍历的算法
// 倒置
const postorder = (root) => {
	if(!root) return
  const outputStack = [] 
  const stack = [root]
  while (stack.length) {
    const n = stack.pop();
    outputStack.push(n)
    if (n.left) stack.push(n.left);
    if (n.right) stack.push(n.right);
  }
  while(outputStack.length) {
    const n = outputStack.pop()
    console.log(n.val)
  }
}

// postorder(bt)