// 批量模块化
function fn() {
  console.log('fn')
}

const name = 'a'

export {
  fn,
  name
}

// 使用 default
// export default{
//   fn,
//   name
// }

// // 引入的时候就不能使用解构赋值
// import b from "./b";
// console.log(b.fn)
// console.log(b.name)
