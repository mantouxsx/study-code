console.log('hello webpack')

// 模块化
// import { fn, name } from "./a"; // 解构赋值
import { fn, name } from "./b";
console.log(fn)
console.log(name)

import xxx from './c';
console.log(xxx)

// 使用 babel 将 es6 转译成 es5
const sum = (a, b) => {
  return a + b
}
const res = sum(10, 20)
console.log(res)