// 正式环境打包
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  // 模式
  mode: 'production',
  // 入口
  entry: path.join(__dirname, 'src', 'index.js'), // __dirname 表示当前目录
  // 输出
  output: {
    filename: 'bundle.[contenthash].js', // 生成一个 hash 值，如果代码改变了文件名则会改变，与 http缓存 有关
    path: path.join(__dirname, 'dist')
  },
  // 模块
  module: {
    rules: [
      // 将 js 文件使用 babel 转换，查看源代码可以看到 sum 函数已经变成 function
      {
        test: /\.js$/, // 校验js
        use: 'babel-loader',
        include: path.join(__dirname, 'src'),
        exclude: /node_modules/
      }
    ]
  },
  // 插件
  plugins: [
    // 打包HTML
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
      filename: 'index.html' // 产出的文件名称
    })
  ]
}