const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  // 模式
  mode: 'development', // production 代码就会被压缩
  // 入口
  entry: path.join(__dirname, 'src', 'index.js'), // __dirname 表示当前目录
  // 输出
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist')
  },
  // 模块
  module: {
    rules: [
      // 将 js 文件使用 babel 转换，查看源代码可以看到 sum 函数已经变成 function
      {
        test: /\.js$/, // 校验js
        use: 'babel-loader',
        include: path.join(__dirname, 'src'),
        exclude: /node_modules/
      }
    ]
  },
  // 插件
  plugins: [
    // 打包HTML
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
      filename: 'index.html' // 产出的文件名称
    })
  ],
  devServer: {
    port: 3000, 
    // webpack 5 delete contentBase, can use static
    // https://blog.csdn.net/qq_45487080/article/details/120471036
    // contentBase: path.join(__dirname, 'dist')
    static: {
      directory: path.join(__dirname, 'dist')
    }
  }
}